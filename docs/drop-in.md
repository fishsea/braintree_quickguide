# Drop-in UI

Drop-in UI is the simplest way of integrating Braintree. In this quick guide, we cover the basic steps for integrating Drop-in UI in web applications. The code samples are in javascript and ruby.

**Demo**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#demo>

**Documentation**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v2>
* <https://developers.braintreepayments.com/guides/drop-in/javascript/v2>

## Flow

1. Client side requests for the payment page
2. Server side generates a _client_token_, and returns the payment page with the client_token
3. Client side initializes Braintree client SDK, and renders the Drop-in UI form
4. Customer fills in credit card information, and submits the form
5. Braintree client SDK intercepts the form submission, and sends the credit card information to Braintree server, and receives a _payment_method_nonce_
6. Braintree client SDK inserts the _payment_method_nonce_ to the form as a hidden input, and resumes the form submission
7. Server side receives the form data containing the _payment_method_nonce_
8. Server side invokes Braintree API _Transaction.sale_ to create a new transaction
9. Braintree processes the transaction, and returns the response

**Documentation**

* <https://developers.braintreepayments.com/start/overview#how-it-works>
* Client Token - <https://developers.braintreepayments.com/reference/request/client-token/generate/php>
* Payment Method Nonce - <https://developers.braintreepayments.com/start/hello-client/javascript/v2#send-payment-method-nonce-to-server>


## Use Case - New Customer Creating a Transaction

### Generate Client Token
A client token is used for setting up the Drop-in UI, and each time the Drop-in UI is loaded, a new client token must be generated.

A client token is generated at the server side, and is made available at the client side for initilization of the client side SDK.

To generate a client token for a new customer:

```ruby
get "/client_token" do
  @client_token = Braintree::ClientToken.generate
end
```

 **NOTE** : The code snippet above allows the client side to obtain a token via Ajax call. Alternatively, the client token can be stored in a page variable which will be rendered together with the payment form.

**Documentation**

* <https://developers.braintreepayments.com/start/overview#client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#get-a-client-token>
* <https://developers.braintreepayments.com/reference/request/client-token/generate/ruby>

### Client Side

#### include braintree.js


	<script src="https://js.braintreegateway.com/js/braintree-2.23.0.min.js"></script>


**Documentation**

* <https://developers.braintreepayments.com/guides/client-sdk/javascript/v2#use-the-direct-link>

#### Setup

	<form id="checkout" method="post" action="/checkout">
	  <div id="payment-form"></div>
	  <input type="submit" value="Pay $10">
	</form>

	<script>
	  braintree.setup(
	    "<%= your_client_token %>",
	    "dropin", {
	      container: "payment-form",
	      dataCollector: {
		    kount: {environment: 'sandbox'}
		  },
		  onReady: function (braintreeInstance) {
		    var form = document.getElementById('payment-form');
		    var deviceDataInput = form['device_data'];

		    if (deviceDataInput == null) {
		      deviceDataInput = document.createElement('input');
		      deviceDataInput.name = 'device_data';
		      deviceDataInput.hidden = true;
		      form.appendChild(deviceDataInput);
		    }

		    deviceDataInput.value = braintreeInstance.deviceData;
		  }
	    });
	</script>

**Documentation**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#setup>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#get-a-client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#send-payment-method-nonce-to-server>
* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/client-side/javascript/v2#collecting-device-data>

### Server Side

#### Create a new transaction

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true,
	  :store_in_vault_on_success => true
	},
	:device_data => params[:device_data]
)
```

* **[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success)** - when this option is enabled, a new customer record and the payment method used for this transaction will be saved into Braintree vault. Braintree will generate a customer id and a payment method token and return them in the response.
* **[submit_for_settlement](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.submit_for_settlement)** - enable this option to deduct fund from customer's credit card. Refer to <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement> for more information.

**Documentation**

* <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement>


#### Process the response

If the transaction is successful, the details of the transaction will be returned as part of the response.

```ruby
if result.success?
	transaction = result.transaction
	id = transaction.id
	braintree_customer_id = transaction.customer.id
	payment_method_token = transaction.payment_method_token

	#
	# save transaction id, customer id, payment method token
	#
end
```

* **[transaction id](https://developers.braintreepayments.com/reference/response/transaction/ruby#id)** - unique id of transaction
* **[customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id)** - When saving the customer details to vault, vault will generate a customer id. You can create a new _braintree_customer_id_ column in the Users table, and save the generated _braintree_customer_id_.
* **[payment method token](https://developers.braintreepayments.com/reference/response/transaction/ruby#credit_card_details.token)** - The payment method saved to vault will be assigned a payment method token

If the transaction is unsuccessful, you should check the error response code and handle the error properly. We recommend returning a generic error message to the customer and ask the customer to try again.

```ruby
if !result.success?
	response_code = result.error
	...
end
```

## Use Case - Existing customer creating a transaction

An existing customer is already associated with a Braintree [customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id), and vaulted payment methods.

### Generate Client Token

The client token is generated using the Braintree customer id, such that vaulted payment methods can be retrieved.

```ruby
get "/client_token" do
  @client_token = Braintree::ClientToken.generate(
	:customer_id => <a_customer_id>
  )
end
```

### Client Side

No change is required on the client side.

### Server Side

#### Create a new transaction

The only difference is the [store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success) does not need to be enabled this time.

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true
	},
	:device_data => params[:device_data]
)
```

#### Process the response

There is no need to retrieve customer id and payment method token.

```ruby
if result.success?
	transaction = result.transaction
	id = transaction.id

	#
	# save transaction id, customer id, payment method token
	# process transaction data
	#
end
```

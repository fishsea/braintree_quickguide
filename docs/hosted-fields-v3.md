# Hosted Fields v3 (Recommended)

Hosted Fields allows customizable payment experience with minimum PCI compliance requirements. The code samples are in javascript and ruby.

**Documentation** 

* <https://developers.braintreepayments.com/guides/hosted-fields/overview/javascript/v3>
* <https://www.braintreepayments.com/blog/introducing-hosted-fields-a-pci-solution-that-doesnt-compromise-your-control/>

## Flow

The flow is very similar to the Drop-in UI flow, except that the payment form is using hosted fields instead of Drop-in UI. Refer to [Drop-in UI](drop-in.md#flow) for details.


## Use Case - New Customer Creating a Transaction

### Generate Client Token
A client token is used for setting up the Hosted Fields payment form, and each time the payment form is loadis loaded, a new client token must be generated.

A client token is generated at the server side, and is made available at the client side for initilization of the client side SDK.

To generate a client token for a new customer:

```ruby
get "/client_token" do
  Braintree::ClientToken.generate
end
```

 **NOTE** : The code snippet above allows the client side to obtain a token via Ajax call. Alternatively, the client token can be stored in a page variable which will be rendered together with the payment form.

**Documentation**

* <https://developers.braintreepayments.com/start/overview#client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v3#get-a-client-token>
* <https://developers.braintreepayments.com/reference/request/client-token/generate/ruby>

### Client Side

#### include braintree.js

    <!-- Load the Client component. -->
    <script src="https://js.braintreegateway.com/web/3.6.2/js/client.min.js"></script>

    <!-- Load the Hosted Fields component. -->
    <script src="https://js.braintreegateway.com/web/3.6.2/js/hosted-fields.min.js"></script>

**Documentation**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v3>

#### Setup 

```html
<form id="checkout-form" action="/transaction-endpoint" method="post">
  <div id="error-message"></div>

    <label for="card-number">Card Number</label>
    <div class="hosted-field" id="card-number"></div>

    <label for="cvv">CVV</label>
    <div class="hosted-field" id="cvv"></div>

    <label for="expiration-date">Expiration Date</label>
    <div class="hosted-field" id="expiration-date"></div>

    <input type="hidden" name="payment-method-nonce">
    <input type="submit" value="Pay $10" disabled>
</form>

<script>
// We generated a client token for you so you can test out this code
// immediately. In a production-ready integration, you will need to
// generate a client token on your server (see section below).
var authorization = 'eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJjZmVkMmY0NDY1MzMzNDU5NTM0NDY1OTg4OTUyNDAwODM1ZDBkZmJiNjdiYTIwMDMxY2ZkNGMxZDQ3NDU5MWE0fGNyZWF0ZWRfYXQ9MjAxNi0xMS0wOVQwODozOToyNC4wNDQzNjQ2MjArMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJjb2luYmFzZUVuYWJsZWQiOmZhbHNlLCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=';
var submit = document.querySelector('input[type="submit"]');

braintree.client.create({
  authorization: authorization
}, function (clientErr, clientInstance) {
  if (clientErr) {
    // Handle error in client creation
    return;
  }

  braintree.hostedFields.create({
    client: clientInstance,
    styles: {
      'input': {
        'font-size': '14pt'
      },
      'input.invalid': {
        'color': 'red'
      },
      'input.valid': {
        'color': 'green'
      }
    },
    fields: {
      number: {
        selector: '#card-number',
        placeholder: '4111 1111 1111 1111'
      },
      cvv: {
        selector: '#cvv',
        placeholder: '123'
      },
      expirationDate: {
        selector: '#expiration-date',
        placeholder: '10/2019'
      }
    }
  }, function (hostedFieldsErr, hostedFieldsInstance) {
    if (hostedFieldsErr) {
      // Handle error in Hosted Fields creation
      return;
    }

    submit.removeAttribute('disabled');

    form.addEventListener('submit', function (event) {
      event.preventDefault();

      hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
        if (tokenizeErr) {
          // Handle error in Hosted Fields tokenization
          return;
        }

        // Put `payload.nonce` into the `payment-method-nonce` input, and then
        // submit the form. Alternatively, you could send the nonce to your server
        // with AJAX.
        document.querySelector('input[name="payment-method-nonce"]').value = payload.nonce;
        form.submit();
      });
    }, false);

  });
});
</script>
```

```css
#card-number {
  border: 1px solid #333;
  -webkit-transition: border-color 160ms;
  transition: border-color 160ms;
}

#card-number.braintree-hosted-fields-focused {
  border-color: #777;
}

#card-number.braintree-hosted-fields-invalid {
  border-color: tomato;
}

#card-number.braintree-hosted-fields-valid {
  border-color: limegreen;
}

```

**Documentation**

* Setup - <https://developers.braintreepayments.com/guides/hosted-fields/setup-and-integration/javascript/v3>
* Configuration Options - <https://braintree.github.io/braintree-web/3.6.2/index.html>
* Styling - <https://developers.braintreepayments.com/guides/hosted-fields/styling/javascript/v3>
* Events - <https://developers.braintreepayments.com/guides/hosted-fields/events/javascript/v3>


### Server Side

#### Create a new transaction

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true,
	  :store_in_vault_on_success => true
	},
	:device_data => params[:device_data]
)
```

* **[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success)** - when this option is enabled, a new customer record and the payment method used for this transaction will be saved into Braintree vault. Braintree will generate a customer id and a payment method token and return them in the response.
* **[submit_for_settlement](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.submit_for_settlement)** - enable this option to deduct fund from customer's credit card. Refer to <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement> for more information.

**Documentation**

* <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement>



#### Process the response

If the transaction is successful, the details of the transaction will be returned as part of the response.

```rails
if result.success?
	transaction = result.transaction
	id = transaction.id
	braintree_customer_id = transaction.customer.id
	payment_method_token = transaction.payment_method_token
	
	#
	# save transaction id, customer id, payment method token
	#
end
```

* **[transaction id](https://developers.braintreepayments.com/reference/response/transaction/ruby#id)** - unique id of transaction
* **[customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id)** - When saving the customer details to vault, vault will generate a customer id.
* **[payment method token](https://developers.braintreepayments.com/reference/response/transaction/ruby#credit_card_details.token)** - The payment method saved to vault will be assigned a payment method token

If the transaction is unsuccessful, you should check the error response code and handle the error properly. We recommend returning a generic error message to the customer and ask the customer to try again.

```ruby
if !result.success?
	response_code = result.error
	...
end
```

## Use Case - Existing customer creating a transaction

An existing customer is already associated with a Braintree [customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id), and vaulted payment methods.

### Generate Client Token

The client token is generated using the Braintree customer id, such that vaulted payment methods can be retrieved.

```rails
get "/client_token" do
  @client_token = Braintree::ClientToken.generate(
	:customer_id => <a_customer_id>
  )
end
```

### Display the Payment Methods

At the time of payment, display a default payment method which will be used for payment, and provide an option for customer to change to other vaulted payment methods.

To obtain all available payment methods, on the server side:

```ruby
# Retrieve a Customer object
customer = Braintree::Customer.find("a_customer_id")

# Final all available payment methods of the customer
@payment_methods = customer.payment_methods
```

Display the payment method on client side:

```erb
<ul>
	<% @payment_methods.each do |payment_method| %>
		<li>
			<%= radio_button_tag(:payment_method, payment_method.token) %>
			<%= image_tag payment_method.image_url %>
			<% case payment_method 
				when Braintree::CreditCard %>
				<%= "#{payment_method.last_4} #{payment_method.expiration_month}/#{payment_method.expiration_year}" %>
			<% 	when Braintree::PayPalAccount %>
				<%= "#{payment_method.email}" %>
			<% end %>
		</li>
	<% end %>
</ul>
```

**Documentation**

* **Customer.find()** - <https://developers.braintreepayments.com/reference/request/customer/find/ruby>
* **Customer** - <https://developers.braintreepayments.com/reference/response/customer/ruby>
* **PaymentMethod** - <https://developers.braintreepayments.com/reference/response/payment-method/ruby>

### Server Side

#### Create a new transaction

A new transaction can be created using the payment method token that the customer has selected. The [store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success) does not need to be enabled this time.

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_token => <payment method token selected>,
	:options => {
	  :submit_for_settlement => true
	},
	:device_data => params[:device_data]
)
```

#### Process the response

There is no need to retrieve customer id and payment method token.

```ruby
if result.success?
	transaction = result.transaction
	id = transaction.id
	
	#
	# save transaction id
	# process transaction data
	#
end
```


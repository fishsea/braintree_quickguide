# Kount Custom Integration

## Kount Merchant ID

Once you sign up for Kount Direct, Braintree will provide a **Kount Merchant ID**. The **Kount Merchant ID** is the same in Kount's sandbox and production environments.

## Client Side - Device Fingerprint Collection

```html
<script src="https://js.braintreegateway.com/web/3.6.2/js/client.min.js"></script>
<script src="https://js.braintreegateway.com/web/3.6.2/js/data-collector.min.js"></script>

<script>
//**** NOTED Your Direct Kount MID should be configured in your Braintree Control Panel, Please reach out to support@braintreepayments.com **** 
braintree.client.create({
  authorization: 'CLIENT_AUTHORIZATION'
}, function (err, clientInstance) {
  // Creation of any other components...

  braintree.dataCollector.create({
    client: clientInstance,
    kount: true
  }, function (err, dataCollectorInstance) {
    if (err) {
      // Handle error in creation of data collector
      return;
    }
    // At this point, you should access the dataCollectorInstance.deviceData value and provide it
    // to your server, e.g. by injecting it into your form as a hidden input.
    var form = document.getElementById('my-form-id');
    var deviceDataInput = form['device_data'];

    if (deviceDataInput == null) {
      deviceDataInput = document.createElement('input');
      deviceDataInput.name = 'device_data';
      deviceDataInput.type = 'hidden';
      form.appendChild(deviceDataInput);
    }

    deviceDataInput.value = dataCollectorInstance.deviceData;
  });
});
</script>
```

**NOTE**: There are native mobile integrations available. Please refer to the official documentation.

**Documentation**

* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/client-side/javascript/v3#kount-custom-integration>

## Server Side - device_data

1. Create transaction using **payment_method_nonce**

		result = Braintree::Transaction.sale(
		  :amount => "1000.00",
		  :payment_method_nonce => nonce_from_the_client,
		  :options => {
		    :submit_for_settlement => false
		  },
		  :device_data => params[:device_data],
          :customer => {
            :firstName => "Drew",
            :lastName => "Smith",
            :company => "Braintree",
            :phone => "312-555-1234",
            :website => "http://www.example.com",
            :email => "drew@example.com"
          },
          billing: {
            :firstName => "Paul",
            :lastName => "Smith",
            :company => "Braintree",
            :streetAddress => "1 E Main St",
            :extendedAddress => "Suite 403",
            :locality => "Chicago",
            :region => "IL",
            :postalCode => "60622",
            :countryCodeAlpha2 => "US"
          },
          shipping: {
            :firstName => "Paul",
            :lastName => "Smith",
            :company => "Braintree",
            :streetAddress => "1 E Main St",
            :extendedAddress => "Suite 403",
            :locality => "Chicago",
            :region => "IL",
            :postalCode => "60622",
            :countryCodeAlpha2 => "US"
          }
		)

2. Create transaction using **payment_method_token**

		result = Braintree::Transaction.sale(
		  :amount => "1000.00",
		  :payment_method_token => paypal_method_token,
		  :options => {
		    :submit_for_settlement => false
		  },
		  :device_data => params[:device_data],
          :billing_address_id => billing_address_id,
          :shipping_address_id => shipping_address_id
		)

3. Vaulting Card

        result = Braintree::PaymentMethod.create(
          :customer_id => "131866",
          :payment_method_nonce => nonce_from_the_client,
          :options => {
            :verify_card => true
          },
          :device_data => params[:device_data],
          :billing_address => {
            :firstName => "Paul",
            :lastName => "Smith",
            :company => "Braintree",
            :streetAddress => "1 E Main St",
            :extendedAddress => "Suite 403",
            :locality => "Chicago",
            :region => "IL",
            :postalCode => "60622",
            :countryCodeAlpha2 => "US"
          }
        )

**Documentation**

* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/server-side/ruby#using-device-data>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#billing>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#billing_address_id>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#customer>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#customer_id>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#shipping>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#shipping_address_id>

## Enable Advanced Fraud Tools

In Braintree gateway, enable the Advanced Fraud Tools option：Settings -> Processing -> Advanced Fraud Tools

![Advanced Fraud Tools](img/config-advanced-fraud-tools.png)

## User Defined Fields (UDF)

Kount supports User Defined Fields (UDF) for risk rules. The value of the UDFs can be passed from Braintree API. You must define UDFs in both Braintree gateway and Kount.

### Naming Convention for UDFs in Braintree

Field Type      | Prefix           | UDF Name     | Result
------------ | ------------- | ------------ | ------------
Text | fraud_  | name | fraud_name
Numeric | fraud_numeric_  | customer_id | fraud_numeric_customer_id
Date | fraud_date_  | order_date | fraud_date_order_date

* Date Format： MM/DD/YYYY

### Create UDFs in Braintree

1. Settings -> Processing

	![Processing Option](img/config-settings-processing.png)

2. In "Custom Fields" Section, Click "New"

	![Custom Field Configuration](img/config-custom-fields.png)

3. Enter UDF information

	* **Api name**: field name without space
	* **Display name**: label
	* **Store and Pass Back**: enabled

	![New Custom Field](img/config-custom-fields-new.png)

### Create UDFs in Kount

Create the same UDFs in Kount, but omit the prefix **fraud_**.

1. Login -> FRAUD CONTROL -> User Defined Fields
	![User Defined Fields Menu](img/kount-config-udf-menu.png)
2. Go to the bottom of the list of User Defined Fields
	![List of User Defined Fields](img/kount-config-udf-list.png)
	![Button to Add User Defined Fields](img/kount-config-udf-add.png)
3. Enter UDF information
	* **Label**: enter the API name of the corresponding Braintree custom field, but omit the prefix **fraud_**
	* **Discription**: description of the field
	* **Type**: field data type

	![Enter UDF information](img/kount-config-udf-new.png)

### Specify UDFs in API Call

```
result = Braintree::Transaction.sale(
  :amount => "1000.00",
  :payment_method_nonce => nonce_from_the_client,
  :options => {
    :submit_for_settlement => false
  },
  :custom_fields => {
    :fraud_name => customer_name,
    :fraud_numeric_customer_id => customer_id,
    :fraud_date_order_date => order_date
  }
  :device_data => params[:device_data]
)
```

## Risk Control Work Flow

Kount returns 4 possible results after evaluating a transaction:

* **Not Evaluated** - Kount did not evalute the transaction (e.g., due to timeout). Braintree will approve the transaction
* **Approve** - Kount approved the transaction. Braintree will approve the transaction
* **Review/Escalate** - Kount recommended to review the transaction. Braintree will approve the transaction but the transaction should be flagged for manual review by merchant
* **Decline** - Kount recommended to decline the transaction. Braintree will decline the transaction.

### Flow Diagram
[![Kount Direct 流程图](img/kount-flow.png)](img/kount-flow.png)

Pseudo Code for the Workflow:

```ruby
# Authorize transaction
result = Braintree::Transaction.sale(
  :amount => "1000.00",
  :payment_method_nonce => nonce_from_the_client,
  :options => {
    :submit_for_settlement => false
  },
  :custom_fields => {
    :fraud_name => customer_name,
    :fraud_numeric_customer_id => customer_id,
    :fraud_date_order_date => order_date
  }
  :device_data => params[:device_data]
)

if result.success?
	transaction_id = result.transaction.id

	case result.transaction.status
	when 'authorized':	# authorization successful, check Kount decision
		decision = result。transaction.risk_data.decision

		# process based on Kount decision
		case decision
		when 'Approve', 'Not Evaluated' # approve the transaction, submit it for settlement
			settlement_result = Braintree::Transaction.submit_for_settlement(transaction_id)
			...
		when 'Review', 'Escalate'       # manual review
			# set order status to "ON HOLD"
			# send a notification to risk team for review
			...
		else
			# handle other statuses
			# Kount Decision = 'Declined' is not possible here since the transaction would have failed and the status would not be 'authorized'
			# Kount Decision could be empty because Kount does not support certain payment methods such as ApplePay etc.

		end
	when 'failed', 'gateway_rejected', 'processor_declined'
		# failed transactions
		# process order
		...
	end
end
```

```ruby
# after manual review
review_result = order.review_result # retrieve manual review decision
transaction = order.transaction     # retrieve payment information
transaction_id = transaction.id

case review_result
when 'APPROVE'    # transaction is approved, submit it for settlement
	settlement_result = Braintree::Transaction.submit_for_settlement(transaction_id)
	...
when 'DECLINE'    # transaction is declined, void the original authorized transaction
	void_result = Braintree::Transaction.void(transaction_id)
	...
else
	# process other manual review decisions
end
```

## Miscellaneous

### Customer email address

if customer email address is empty, do not pass the parameter [customer_email](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#customer.email).

**Documentation**

* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/server-side/ruby#customer-email-address>

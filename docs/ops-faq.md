# Operations FAQ

### What is the description on customer's credit card statement?
It's the merchant name submitted at the time of Braintree application.

### Chargeback process
Braintree will send an email to an email address when a chargeback is filed. The email address is the one provided at the time of Braintree application. A merchant needs to follow the instructions in the email to provide evidence when contesting chargebacks.


# Braintree Go-live Checklist

## HTTPS

* Make sure payment payges are protected by HTTPS to be PCI compliant. Use only TLS 1.1 or 1.2.

## AVS Rules

* Turn on the following AVS rules only for US cards (Note that [billing country](https://developer.braintreepayments.com/reference) should be set to US).

## CVV Rules

* Turn on the following CVV Rules
	* Rule 1
	* Rule 2

## Risk Threshold Rules

* You should customize these rules
* Start with the following rules

## Advanced fraud tools

* integrate braintree-data.js in the client side
* include device_data parameter in Transaction.sale()



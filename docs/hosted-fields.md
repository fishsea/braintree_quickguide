# Hosted Fields

Hosted Fields allows customizable payment experience with minimum PCI compliance requirements. The code samples are in javascript and ruby.

**Documentation** 

* <https://developers.braintreepayments.com/guides/hosted-fields/overview>
* <https://www.braintreepayments.com/blog/introducing-hosted-fields-a-pci-solution-that-doesnt-compromise-your-control/>

## Flow

The flow is very similar to the Drop-in UI flow, except that the payment form is using hosted fields instead of Drop-in UI. Refer to [Drop-in UI](drop-in.md#flow) for details.


## Use Case - New Customer Creating a Transaction

### Generate Client Token
A client token is used for setting up the Hosted Fields payment form, and each time the payment form is loadis loaded, a new client token must be generated.

A client token is generated at the server side, and is made available at the client side for initilization of the client side SDK.

To generate a client token for a new customer:

```ruby
get "/client_token" do
  Braintree::ClientToken.generate
end
```

 **NOTE** : The code snippet above allows the client side to obtain a token via Ajax call. Alternatively, the client token can be stored in a page variable which will be rendered together with the payment form.

**Documentation**

* <https://developers.braintreepayments.com/start/overview#client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#get-a-client-token>
* <https://developers.braintreepayments.com/reference/request/client-token/generate/ruby>

### Client Side

#### include braintree.js

    <script src="https://js.braintreegateway.com/js/braintree-2.23.0.min.js"></script>

**Documentation**

* <https://developers.braintreepayments.com/guides/client-sdk/javascript/v2#use-the-direct-link>

#### Setup 

```html
<form id="checkout" method="post" action="/checkout">
    <div id="paypal-button"></div>
    <div id="card-number" class="bt-input"></div>
    <div id="cvv" class="bt-input"></div>
    <div id="expiration-date" class="bt-input"></div>
	<input type="submit" value="Pay $10">
</form>

<script>
$(function() {
  braintree.setup('<%= your_client_token %>', "custom", {
    id: "new_payment_method",
    hostedFields: {
      onFieldEvent: function (event) {
        if (event.type === "focus") {
          // Handle focus
        } else if (event.type === "blur") {
          // Handle blur
        } else if (event.type === "fieldStateChange") {
          // Handle a change in validation or card type
          console.log(event.isValid); // true|false

          //if (event.isPotentiallyValid)
          console.log("Potentially Valid: " + event.isPotentiallyValid);

          if (!event.isValid) {
            $('.payment-method-icon').removeClass()
                                     .addClass("payment-method-icon");
          }
          if (event.card) {
            console.log(event.card.type);
            // visa|master-card|american-express|diners-club|discover|jcb|unionpay|maestro
            
            $('.payment-method-icon').removeClass()
                                     .addClass("payment-method-icon " + event.card.type);
          }
        }
      },
     styles: {
        // Style all elements
        // these are not all available styles.
        // refer to documentation for more info
        "input": {
          "font-size": "14px",
          "color": "#555555",
          "font-family": "'Helvetica Neue', Helvetica, Arial, sans-serif",
          "font-size": "14px",
          "font-style": "normal",
          "font-variant": "normal",
          "font-weight": "normal"
        },

        // Styling a specific field
        ".number": {
          // "font-family": "monospace"
        },

        // Styling element state
        ":focus": {
          "color": "blue"
        },
        ".valid": {
          "color": "green"
        },
        ".invalid": {
          "color": "red"
        },

        // Media queries
        // Note that these apply to the iframe, not the root window.
        "@media screen and (max-width: 700px)": {
          "input": {
            "font-size": "14px"
          }
        }
    },
      number: {
        selector: "#card-number",
        placeholder: "Credit Card Number"
      },
      cvv: {
        selector: "#cvv",
        placeholder: "CVV"
      },
      expirationDate: {
        selector: "#expiration-date",
        placeholder: "MM/YYYY"
      }
    },
    paypal: {
      container: "paypal-button"
    },
    dataCollector: {
      kount: {environment: 'sandbox'}
    },
    onReady: function (braintreeInstance) {
      var form = document.getElementById('new_payment_method');
      var deviceDataInput = form['device_data'];

      if (deviceDataInput == null) {
        deviceDataInput = document.createElement('input');
        deviceDataInput.name = 'device_data';
        deviceDataInput.hidden = true;
        form.appendChild(deviceDataInput);
      }

      deviceDataInput.value = braintreeInstance.deviceData;
    }
  });
});
</script>
```

```css
#card-number {
  -webkit-transition: border-color 160ms;
  transition: border-color 160ms;
}

.braintree-hosted-fields-focused {
  border-color: blue;
}

.braintree-hosted-fields-invalid {
  border-color: tomato;
}

.braintree-hosted-fields-valid {
  border-color: limegreen;
}

.payment-method-icon {
	background-repeat: no-repeat;
	background-size: 86px auto;
	height: 28px;
	width: 44px;
	display: block;
	margin-top: -31px;
	position: absolute;
	left: auto;
	right: 14px;
	text-indent: -15984px;
}

.visa {
	background-image: url(https://assets.braintreegateway.com/dropin/1.4.0/images/2x-sf9a66b4f5a.png);
	background-position: 0px -184px;
}

.master-card {
	background-image: url(https://assets.braintreegateway.com/dropin/1.4.0/images/2x-sf9a66b4f5a.png);
	background-position: 0px -128px;
}

.american-express {
	// set amex image background
}
```

**Documentation**

* Setup - <https://developers.braintreepayments.com/guides/hosted-fields/setup-and-integration/javascript/v2>
* Configuration Options - <https://developers.braintreepayments.com/guides/hosted-fields/configuration/javascript/v2>
* Styling - <https://developers.braintreepayments.com/guides/hosted-fields/styling/javascript/v2>
* Events - <https://developers.braintreepayments.com/guides/hosted-fields/events/javascript/v2>


### Server Side

#### Create a new transaction

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true,
	  :store_in_vault_on_success => true
	},
	:device_data => params[:device_data]
)
```

* **[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success)** - when this option is enabled, a new customer record and the payment method used for this transaction will be saved into Braintree vault. Braintree will generate a customer id and a payment method token and return them in the response.
* **[submit_for_settlement](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.submit_for_settlement)** - enable this option to deduct fund from customer's credit card. Refer to <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement> for more information.

**Documentation**

* <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement>



#### Process the response

If the transaction is successful, the details of the transaction will be returned as part of the response.

```rails
if result.success?
	transaction = result.transaction
	id = transaction.id
	braintree_customer_id = transaction.customer.id
	payment_method_token = transaction.payment_method_token
	
	#
	# save transaction id, customer id, payment method token
	#
end
```

* **[transaction id](https://developers.braintreepayments.com/reference/response/transaction/ruby#id)** - unique id of transaction
* **[customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id)** - When saving the customer details to vault, vault will generate a customer id.
* **[payment method token](https://developers.braintreepayments.com/reference/response/transaction/ruby#credit_card_details.token)** - The payment method saved to vault will be assigned a payment method token

If the transaction is unsuccessful, you should check the error response code and handle the error properly. We recommend returning a generic error message to the customer and ask the customer to try again.

```ruby
if !result.success?
	response_code = result.error
	...
end
```

## Use Case - Existing customer creating a transaction

An existing customer is already associated with a Braintree [customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id), and vaulted payment methods.

### Generate Client Token

The client token is generated using the Braintree customer id, such that vaulted payment methods can be retrieved.

```rails
get "/client_token" do
  @client_token = Braintree::ClientToken.generate(
	:customer_id => <a_customer_id>
  )
end
```

### Display the Payment Methods

At the time of payment, display a default payment method which will be used for payment, and provide an option for customer to change to other vaulted payment methods.

To obtain all available payment methods, on the server side:

```ruby
# Retrieve a Customer object
customer = Braintree::Customer.find("a_customer_id")

# Final all available payment methods of the customer
@payment_methods = customer.payment_methods
```

Display the payment method on client side:

```erb
<ul>
	<% @payment_methods.each do |payment_method| %>
		<li>
			<%= radio_button_tag(:payment_method, payment_method.token) %>
			<%= image_tag payment_method.image_url %>
			<% case payment_method 
				when Braintree::CreditCard %>
				<%= "#{payment_method.last_4} #{payment_method.expiration_month}/#{payment_method.expiration_year}" %>
			<% 	when Braintree::PayPalAccount %>
				<%= "#{payment_method.email}" %>
			<% end %>
		</li>
	<% end %>
</ul>
```

**Documentation**

* **Customer.find()** - <https://developers.braintreepayments.com/reference/request/customer/find/ruby>
* **Customer** - <https://developers.braintreepayments.com/reference/response/customer/ruby>
* **PaymentMethod** - <https://developers.braintreepayments.com/reference/response/payment-method/ruby>

### Server Side

#### Create a new transaction

A new transaction can be created using the payment method token that the customer has selected. The [store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success) does not need to be enabled this time.

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_token => <payment method token selected>,
	:options => {
	  :submit_for_settlement => true
	},
	:device_data => params[:device_data]
)
```

#### Process the response

There is no need to retrieve customer id and payment method token.

```ruby
if result.success?
	transaction = result.transaction
	id = transaction.id
	
	#
	# save transaction id
	# process transaction data
	#
end
```


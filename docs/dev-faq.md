# Development FAQ

## How to bypass Braintree form validation when another payment method is chosen

Submit the form in the Braintree [onError](https://developers.braintreepayments.com/guides/client-sdk/javascript/v2#global-setup) callback.

```aavascript
braintree.setup('your_token', "custom", {
    id: "form_id",
    onError: function(error) {
      isBraintree = isBraintreePayment();
      if (isBraintree == false) {
        var form = document.getElementById('form_id');
        HTMLFormElement.prototype.submit.call(form);
      }
    },
    ...
}
```

## How to add presentment currencies in sandbox

Refer to <https://articles.braintreepayments.com/get-started/currencies#testing-in-multiple-currencies>
